package tasks;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ArrayListTests {

	@Test
	void addTest() {
		ArrayList arrayList = new ArrayList();
		arrayList.add(1);
		assertEquals(1,arrayList.array[0]);
		arrayList.add(2);
		assertEquals(2,arrayList.array[1]);
	}
	
	@Test
	void getSizeTest() {
		ArrayList arrayList = new ArrayList();
		arrayList.add(1);
		assertEquals(1,arrayList.getSize());
	}

}
