package tasks;

public class ArrayList {
	int array[];
	int capacity;	// Size of the array including empty spaces
	int current;	// Moving pointer to save progress during addition of elements
	
	public ArrayList() {
		this.array = new int[32];	// Initializing size with 32 element
		this.capacity = 32;	// Updating the capacity variable
		this.current= 0;	// Updating the current pointer
	}
	
	public void add(int value) {
		if(this.current < this.capacity) {	// if the pointer doesn't exceed the capacity, perform addition of the new element normally
			this.array[this.current] = value;
			this.current= this.current + 1;
		}
		else {	// if not, please extend first, check extend() down below
			this.extend();
			this.array[this.current] = value;	// Then add normally
			this.current= this.current + 1;		// Update my pointer
		}
	}
	
	// Get number of elements in the arrayList
	public int getSize() {
		return this.current;
	}
	
	// Get number of memory slots occupied by the arrayList (Including empty slots)
	public float getCapacity() {
		return this.capacity;
	}
	
	// Returns the Index specified element from the array
	public int getElement(int index) {
		return this.array[index];
	}
	
	// Copies the old array into the newly bigger sized one and updates the array attribute with the newly created one
	private void extend() {
		int newArray[] = new int[this.capacity+32];
		for (int i=0; i < this.capacity; i++) {
			newArray[i] = this.array[i];
		}
		this.array = newArray;
		this.capacity = this.capacity + 32;
		System.out.println("Extended with new capacity of = " + newArray.length);
	}
	
	
	
	public static void main(String[] args) {
		
		// Demo
		
		ArrayList myArrayList = new ArrayList();
		
		for (int i=0; i < 35 /* << Change this magic number to try different sizes */; i++) {
			myArrayList.add(50);
			System.out.println("Value at index #"+i+" : "+myArrayList.getElement(i));
			System.out.println("Current size: "+myArrayList.getSize());
		}
	}
}
